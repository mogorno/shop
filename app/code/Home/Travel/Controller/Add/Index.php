<?php

namespace Home\Travel\Controller\Add;

use Home\Travel\Controller\ControllerAbstract;

class Index extends ControllerAbstract
{
    protected $pageTitle = 'Add cruise';
}