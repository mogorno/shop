<?php

namespace Home\Travel\Controller\Add;

use Home\Travel\Api\Data\TravelInterface;
use Home\Travel\Api\TravelRepositoryInterface;
use Home\Travel\Controller\ControllerAbstract;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\View\Result\Page;
use Magento\Framework\View\Result\PageFactory;

class Save extends ControllerAbstract
{
    /**
     * @var TravelRepositoryInterface
     */
    protected $travelFactory;

    /**
     * @var TravelInterface
     */
    protected $travel;

    /**
     * Save constructor.
     * @param Context $context
     * @param PageFactory $pageFactory
     * @param TravelRepositoryInterface $travelFactory
     * @param TravelInterface $travel
     */
    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        TravelRepositoryInterface $travelFactory,
        TravelInterface $travel
    ) {
        parent::__construct($pageFactory, $context);

        $this->travelFactory = $travelFactory;
        $this->travelFactory = $travelFactory;
        $this->travel = $travel;
    }

    /**
     * @return ResponseInterface|ResultInterface|Page
     * @throws LocalizedException
     */
    public function execute()
    {
        if ($this->getRequest()->isPost()) {
            $params = $this->getRequest()->getParams();

            $travel = $this->travel
                ->setId($params['ticket_id'])
                ->setTitle($params['title'])
                ->setPrice($params['price'])
                ->setAirportId($params['airport_id'])
                ->setAirportFromId($params['airport_from_id'])
                ->setTimeStart($params['time_start'])
                ->setTimeFinish($params['time_finish']);
            $this->travelFactory->save($travel);

            return $this->_redirect('travel');
        }
    }
}

