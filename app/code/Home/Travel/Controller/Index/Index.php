<?php

namespace Home\Travel\Controller\Index;

use Home\Travel\Controller\ControllerAbstract;

class Index extends ControllerAbstract
{
    protected $pageTitle = 'All cruise';
}
