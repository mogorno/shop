<?php

namespace Home\Travel\Controller\Update;

use Home\Travel\Api\Data\TravelInterface;
use Home\Travel\Api\TravelRepositoryInterface;
use Home\Travel\Controller\ControllerAbstract;
use Home\Travel\Model\TravelRepository;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Save extends ControllerAbstract
{
    /**
     * @var TravelRepositoryInterface
     */
    protected $travelFactory;

    /**
     * @var TravelRepository
     */
    protected $travel;

    /**
     * Save constructor.
     * @param Context $context
     * @param PageFactory $resultFactory
     * @param TravelRepositoryInterface $travelFactory
     * @param TravelInterface $travel
     */
    public function __construct(
        Context $context,
        PageFactory $resultFactory,
        TravelRepositoryInterface $travelFactory,
        TravelInterface $travel
    )
    {
        parent::__construct($resultFactory, $context);

        $this->travelFactory = $travelFactory;
        $this->travel = $travel;
    }

    public function execute()
    {
        if ($this->getRequest()->isPost()) {
            $params = $this->getRequest()->getParams();

            $travel = $this->travelFactory
                ->getById($params['ticket_id'])
                ->setTitle($params['title'])
                ->setPrice($params['price'])
                ->setAirportId($params['airport_id'])
                ->setAirportFromId($params['airport_from_id'])
                ->setTimeStart($params['time_start'])
                ->setTimeFinish($params['time_finish']);

            $this->travelFactory->save($travel);

            return $this->_redirect('travel');
        }
    }
}