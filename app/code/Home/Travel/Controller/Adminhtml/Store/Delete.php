<?php

namespace Home\Travel\Controller\Adminhtml\Store;

use Magento\Backend\App\Action\Context;
use Home\Travel\Api\TravelRepositoryInterface;

class Delete extends \Magento\Backend\App\Action
{
    const ADMIN_RESOURCE = "Home_Travel::all";

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $pageFactory;

    /**
     * @var TravelRepositoryInterface
     */
    private $travelRepository;

    /**
     * Constructor
     *
     * @param Context $context
     * @param TravelRepositoryInterface $travelRepository
     */
    public function __construct(
        Context $context,
        TravelRepositoryInterface $travelRepository
    ) {
        parent::__construct($context);
        $this->travelRepository = $travelRepository;
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('ticket_id');
        if ($id) {
            $travel = $this->travelRepository->getById($id);
            try {
                $this->travelRepository->delete($travel);
                $this->messageManager->addSuccess(__("The ticket was deleted success."));
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage(__("Couldn't deleted the ticket."));
            }
        } else {
            $this->messageManager->addErrorMessage(__("Can't find the ticket."));
        }
        $this->_redirect("travel/store/index");
    }
}
