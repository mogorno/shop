<?php
namespace Home\Travel\Controller\Adminhtml\Store;

use Home\Travel\Api\Data\TravelInterface;
use Home\Travel\Api\Data\TravelInterfaceFactory;
use Home\Travel\Api\TravelRepositoryInterface;
use Magento\Backend\App\Action\Context;

class Save extends \Magento\Backend\App\Action
{
    const ADMIN_RESOURCE = "Home_Travel::all";

    /**
     * @var TravelInterfaceFactory
     */
    private $travelFactory;

    /**
     * @var TravelRepositoryInterface
     */
    private $travelRepository;

    /**
     * Save constructor.
     * @param TravelInterfaceFactory $travelFactory
     * @param TravelRepositoryInterface $travelRepository
     * @param Context $context
     */
    public function __construct(
        TravelInterfaceFactory $travelFactory,
        TravelRepositoryInterface $travelRepository,
        Context $context
    ) {
        parent::__construct($context);
        $this->travelFactory = $travelFactory;
        $this->travelRepository = $travelRepository;
    }

    public function execute()
    {

        $data = $this->getRequest()->getPostValue();
        /** @var TravelInterface $travel */
        $travel = $this->travelFactory->create();
        if (isset($data['ticket_id'])) {
            $travel->setId($data['ticket_id']);
        }
        $travel
            ->setTitle($data['title'])
            ->setPrice($data['price'])
            ->setAirportId($data['airport_id'])
            ->setAirportFromId($data['airport_from_id'])
            ->setTimeStart($data['time_start'])
            ->setTimeFinish($data['time_finish']);
        $this->travelRepository->save($travel);
        $this->messageManager->addSuccess(__("The store was saved success."));
        $this->_redirect("travel/store/index");
    }
}
