<?php

namespace Home\Travel\Controller\Adminhtml\Store;

use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use Home\Travel\Model\ResourceModel\Travel\CollectionFactory;
use Home\Travel\Api\TravelRepositoryInterface;

class MassDelete extends \Magento\Backend\App\Action
{
    const ADMIN_RESOURCE = "Home_Travel::all";

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $pageFactory;

    /**
     * @var Filter
     */
    private $filter;

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var TravelRepositoryInterface
     */
    private $travelRepository;

    /**
     * Constructor
     *
     * @param Context $context
     * @param Filter $filter
     * @param CollectionFactory $collectionFactory
     * @param TravelRepositoryInterface $travelRepository
     */
    public function __construct(
        Context $context,
        Filter $filter,
        CollectionFactory $collectionFactory,
        TravelRepositoryInterface $travelRepository
    )
    {
        parent::__construct($context);
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        $this->travelRepository = $travelRepository;
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute()
    {
        $collection = $this->filter->getCollection($this->collectionFactory->create());
        foreach ($collection as $travel) {
            $this->travelRepository->delete($travel);
        }
        $this->messageManager->addSuccess(__('A total of %1 record(s) have been deleted.', $collection->getSize()));
        $this->_redirect("travel/store/index");
    }
}
