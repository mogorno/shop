<?php

namespace Home\Travel\Controller\Delete;

use Home\Travel\Api\TravelRepositoryInterface;
use Home\Travel\Controller\ControllerAbstract;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Delete extends ControllerAbstract
{
    /**
     * @var TravelRepositoryInterface
     */
    protected $travelFactory;

    /**
     * Delete constructor.
     * @param Context $context
     * @param PageFactory $resultFactory
     * @param TravelRepositoryInterface $travelFactory
     */
    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        TravelRepositoryInterface $travelFactory
    )
    {
        parent::__construct($pageFactory, $context);

        $this->travelFactory = $travelFactory;
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|\Magento\Framework\View\Result\Page
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        $this->travelFactory->deleteById($id);

        return $this->_redirect('travel');
    }
}