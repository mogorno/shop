<?php

namespace Home\Travel\Controller;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

abstract class ControllerAbstract extends Action
{
    /**
     * @var PageFactory
     */
    protected $pageFactory;

    protected $pageTitle = "";

    /**
     * ControllerAbstract constructor.
     * @param PageFactory $pageFactory
     * @param Context $context
     */
    public function __construct(
        PageFactory $pageFactory,
        Context $context
    ) {
        $this->pageFactory = $pageFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $pageFactory = $this->pageFactory->create();
        $pageFactory->getConfig()->getTitle()->set(__($this->pageTitle));

        return  $pageFactory;
    }
}
