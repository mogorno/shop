<?php
namespace Home\Travel\Ui\DataProvider;

use Magento\Ui\DataProvider\AbstractDataProvider;
use Magento\Framework\App\Request\DataPersistorInterface;
use Home\Travel\Model\Travel as TravelModel;
use Home\Travel\Model\ResourceModel\Travel\Collection;
use Home\Travel\Model\ResourceModel\Travel\CollectionFactory;

class TravelDataProvider extends AbstractDataProvider
{
    /**
     * @var Collection
     */
    protected $collection;

    /**
     * @var array
     */
    private $loadedData = [];

    /**
     * @var DataPersistorInterface
     */
    private $dataPersistor;

    /**
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $collectionFactory
     * @param DataPersistorInterface $dataPersistor
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collectionFactory,
        DataPersistorInterface $dataPersistor,
        array $meta = [],
        array $data = []
    )
    {
        $this->collection = $collectionFactory->create();
        $this->dataPersistor = $dataPersistor;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    /**
     * @inheritdoc
     */
    public function getData(): array
    {
        if (!empty($this->loadedData)) {
            return $this->loadedData;
        }

        $items = $this->collection->getItems();

        /** @var TravelModel $travel */
        foreach ($items as $travel) {
            $this->loadedData[$travel->getId()] = $travel->getData();
        }

        $data = $this->dataPersistor->get('travel');
        if (!empty($data)) {
            $travel = $this->collection->getNewEmptyItem();
            $travel->setData($data);
            $this->loadedData[$travel->getId()] = $travel->getData();
            $this->dataPersistor->clear('travel');
        }

        return $this->loadedData;
    }
}