<?php

namespace Home\Travel\Setup2;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    )
    {
        $installer = $setup;

        $installer->startSetup();

        $connection = $installer->getConnection();
        if (version_compare($context->getVersion(), '1.0.4', '<')) {
            $connection->addColumn(
                $installer->getTable('travel_tickets'),
                'time',
                [
                    'type' => Table::TYPE_TIMESTAMP,
                    'length' => '',
                    'nullable' => 'CURRENT_TIMESTAMP',
                    'comment' => 'TIME-DATA'
                ]
            );
        }
        $installer->endSetup();
    }
}