<?php

namespace Home\Travel\Setup;

use Magento\Eav\Setup\EavSetup;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Diggecard\Giftcard\Api\Data\GiftcardInterface;
use Diggecard\Giftcard\Model\ResourceModel\Giftcard as GiftcardResourceModel;
use Diggecard\Giftcard\Api\Data\DiscountInterface;
use Diggecard\Giftcard\Model\ResourceModel\Discount as DiscountResourceModel;
use Magento\Quote\Setup\QuoteSetup;
use Magento\Quote\Setup\QuoteSetupFactory;
use Zend_Db_Exception;
/**
 * Class InstallSchema
 * @package Home\Travel\Setup
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * @var EavSetup
     */
    private $eavSetup;

    /**
     * InstallSchema constructor.
     * @param EavSetup $eavSetup
     */
    public function __construct(
        EavSetup $eavSetup
    ) {
        $this->eavSetup = $eavSetup;
    }

    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @throws Zend_Db_Exception
     */
    public function install(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $installer = $setup;
        $installer->startSetup();

        /**
         * Create table 'travel_tickets'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('travel_tickets')
        )->addColumn(
            'ticket_id',
            Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'Store Id'
        )->addColumn(
            'title',
            Table::TYPE_TEXT,
            36,
            ['nullable' => false],
            'Store Title'
        )->addColumn(
            'price',
            Table::TYPE_INTEGER,
            11,
            ['nullable' => false],
            'Store Price'
        )->addColumn(
            'airport_id',
            Table::TYPE_INTEGER,
            11,
            ['nullable' => true],
            'AID'
        )->addColumn(
            'airport_from_id',
            Table::TYPE_INTEGER,
            11,
            ['nullable' => true],
            'AFID'
        )->addColumn(
            'time_start',
            Table::TYPE_DATE,
            null,
            [],
            'TimeStart'
        )->addColumn(
            'time_finish',
            Table::TYPE_DATE,
            null,
            [],
            'TimeFinish'
        )->setComment('Travel Tickets Table');
        $installer->getConnection()->createTable($table);
        $installer->endSetup();
        /**
         * travel_city
         */
        $installer_city = $setup;
        $installer_city->startSetup();
        /**
         * Create table 'travel_city'
         */
        $table1 = $installer_city->getConnection()->newTable(
            $installer_city->getTable('travel_city')
        )->addColumn(
            'city_id',
            Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'Sity Id'
        )->addColumn(
            'city',
            Table::TYPE_TEXT,
            36,
            ['nullable' => false],
            'City Title'
        )->setComment('Travel City Table');
        $installer_city->getConnection()->createTable($table1);
        $installer_city->endSetup();
        /**
         * travel_airport
         */
        $installer_airport = $setup;
        $installer_airport->startSetup();
        /**
         * Create table 'travel_airport'
         */
        $table2 = $installer_airport->getConnection()->newTable(
            $installer_airport->getTable('travel_airport')
        )->addColumn(
            'airport_id',
            Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'Airport Id'
        )->addColumn(
            'airport_name',
            Table::TYPE_TEXT,
            36,
            ['nullable' => false],
            'Airport Name'
        )->addColumn(
            'city_id',
            Table::TYPE_INTEGER,
            11,
            ['nullable' => false],
            'city id'
        )->setComment('Travel Airport Table');
        $installer_airport->getConnection()->createTable($table2);
        $installer_airport->endSetup();

    }

}
