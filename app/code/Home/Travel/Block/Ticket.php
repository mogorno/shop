<?php

namespace Home\Travel\Block;

use Home\Travel\Api\Data\TravelInterface;
use Home\Travel\Api\TravelRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\View\Element\Template;

/**
 * Class Ticket
 * @package Home\Travel\Block
 */
class Ticket extends Template
{
    /**
     * @var TravelRepositoryInterface
     */
    protected $travel;
    /**
     * @var SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    /**
     * Ticket constructor.
     * @param TravelRepositoryInterface $travel
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param Template\Context $context
     */
    public function __construct(
        TravelRepositoryInterface $travel,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        Template\Context $context
    ) {
        parent::__construct($context);
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->travel = $travel;
    }

    /**
     * @return TravelInterface[]
     * @throws LocalizedException
     */
    public function getTravelList()
    {
        $searchCriteriaBuilder = $this->searchCriteriaBuilder->create();
        $travel = $this->travel->getList($searchCriteriaBuilder)->getItems();
        return $travel;
    }
    /**
     * @return TravelInterface
     * @throws LocalizedException
     */
    public function getTicket()
    {
        $id = $this->getRequest()->getParam('id');
        $travel = $this->travel->getById($id);
        return $travel;
    }

    /**
     * @return TravelRepositoryInterface
     */
    public function getTickets()
    {
        $travel = $this->travel;
        return $travel;
    }
}
