<?php

namespace Home\Travel\DB;

class Tickets
{
    protected $id;
    protected $title;
    protected $price;
    protected $airport_id;
    protected $airport_from_id;
    protected $time_start;
    protected $time_finish;
}