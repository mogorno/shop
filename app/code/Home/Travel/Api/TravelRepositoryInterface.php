<?php
namespace Home\Travel\Api;

interface TravelRepositoryInterface
{
    /**
     * Save travel.
     *
     * @param \Home\Travel\Api\Data\TravelInterface $travel
     * @return \Home\Travel\Api\Data\TravelInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(\Home\Travel\Api\Data\TravelInterface $travel);

    /**
     * Retrieve travel.
     *
     * @param int $travelId
     * @return \Home\Travel\Api\Data\TravelInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($travelId);

    /**
     * Retrieve tickets matching the specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Home\Travel\Api\Data\TravelSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Delete page.
     *
     * @param \Home\Travel\Api\Data\TravelInterface $travel
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(\Home\Travel\Api\Data\TravelInterface $travel);

    /**
     * Delete realty by ID.
     *
     * @param int $travelId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($travelId);
}