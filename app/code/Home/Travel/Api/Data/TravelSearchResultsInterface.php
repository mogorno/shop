<?php

namespace Home\Travel\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

interface TravelSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get pages list.
     *
     * @return \Home\Travel\Api\Data\TravelInterface[]
     */
    public function getItems();

    /**
     * Set pages list.
     *
     * @param \Home\Travel\Api\Data\TravelInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
