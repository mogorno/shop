<?php

namespace Home\Travel\Api\Data;

interface TravelInterface
{
    /**
     * data array
     */
    const TICKET_ID                = 'ticket_id';
    const TITLE                    = 'title';
    const PRICE                    = 'price';
    const AIRPORT_ID               = 'airport_id';
    const AIRPORT_FROM_ID          = 'airport_from_id';
    const TIME_START               = 'time_start';
    const TIME_FINISH              = 'time_finish';

    /**
     *
     *  ****************** GET **************
     *
     */

    /**
     * Get ID
     *
     * @return int
     */
    public function getId();

    /**
     * Get Title
     *
     * @return string
     */
    public function getTitle();

    /**
     * Get Price
     *
     * @return int
     */
    public function getPrice();

    /**
     * Get Airport id
     *
     * @return int
     */
    public function getAirportId();

    /**
     * Get Airport from id
     *
     * @return int
     */
    public function getAirportFromId();

    /**
     * Get Time start
     *
     * @return mixed
     */
    public function getTimeStart();

    /**
     * Get Time finish
     *
     * @return mixed
     */
    public function getTimeFinish();

    /**
     * *********************************  SET   ********************************
     */

    /**
     * Set ID
     *
     * @param int $ticket_id
     * @return \Home\Travel\Api\Data\TravelInterface
     */
    public function setId($ticket_id);

    /**
     * Set Title
     *
     * @param string $title
     * @return \Home\Travel\Api\Data\TravelInterface
     */
    public function setTitle($title);

    /**
     * Set Price
     *
     * @param int $price
     * @return \Home\Travel\Api\Data\TravelInterface
     */
    public function setPrice($price);

    /**
     * Set Airport id
     *
     * @param int $airports_id
     * @return \Home\Travel\Api\Data\TravelInterface
     */
    public function setAirportId($airports_id);

    /**
     * @param int $airports_from_id
     * @return \Home\Travel\Api\Data\TravelInterface
     */
    public function setAirportFromId($airports_from_id);

    /**
     * @param $time_start
     * @return \Home\Travel\Api\Data\TravelInterface
     */
    public function setTimeStart($time_start);

    /**
     * @param $time_finish
     * @return \Home\Travel\Api\Data\TravelInterface
     */

    public function setTimeFinish($time_finish);
}