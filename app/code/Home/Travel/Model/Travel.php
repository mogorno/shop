<?php

namespace Home\Travel\Model;

use Home\Travel\Api\Data\TravelInterface;
use Magento\Framework\Model\AbstractModel;

class Travel extends AbstractModel implements TravelInterface
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Home\Travel\Model\ResourceModel\Travel::class);
    }

    /**
     * Get ID
     *
     * @return int
     */
    public function getId()
    {
        return $this->getData(self::TICKET_ID);
    }

    /**
     * Get Title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->getData(self::TITLE);
    }

    /**
     * Get Price
     *
     * @return int
     */
    public function getPrice()
    {
        return $this->getData(self::PRICE);
    }

    /**
     * Get AirportId
     *
     * @return int
     */
    public function getAirportId()
    {
        return $this->getData(self::AIRPORT_ID);
    }

    /**
     * Get AirportFromId
     *
     * @return int
     */
    public function getAirportFromId()
    {
        return $this->getData(self::AIRPORT_FROM_ID);
    }

    /**
     * Get TimeStart
     *
     * @return mixed
     */
    public function getTimeStart()
    {
        return $this->getData(self::TIME_START);
    }

    /**
     * Get TimeFinish
     *
     * @return mixed
     */
    public function getTimeFinish()
    {
        return $this->getData(self::TIME_FINISH);
    }
    /**
     *
     * ******************************************SET    SET     SET**********************************************************
     *
     */
    /**
     * Set ID
     *
     * @param int $ticket_id
     * @return \Home\Travel\Api\Data\TravelInterface
     */
    public function setId($ticket_id)
    {
        return $this->setData(self::TICKET_ID, $ticket_id);
    }

    /**
     * Set Title
     *
     * @param string $title
     * @return \Home\Travel\Api\Data\TravelInterface
     */
    public function setTitle($title)
    {
        return $this->setData(self::TITLE, $title);
    }

    /**
     * Set PRICE
     *
     * @param int $price
     * @return \Home\Travel\Api\Data\TravelInterface
     */
    public function setPrice($price)
    {
        return $this->setData(self::PRICE, $price);
    }

    /**
     * Set AIRPORT_ID
     *
     * @param int $airport_id
     * @return \Home\Travel\Api\Data\TravelInterface
     */
    public function setAirportId($airport_id)
    {
        return $this->setData(self::AIRPORT_ID, $airport_id);
    }

    /**
     * Set AIRPORT_FROM_ID
     *
     * @param int $airport_from_id
     * @return \Home\Travel\Api\Data\TravelInterface
     */
    public function setAirportFromId($airport_from_id)
    {
        return $this->setData(self::AIRPORT_FROM_ID, $airport_from_id);
    }

    /**
     * Set TimeStart
     *
     * @param mixed $time_start
     * @return \Home\Travel\Api\Data\TravelInterface
     */
    public function setTimeStart($time_start)
    {
        return $this->setData(self::TIME_START, $time_start);
    }

    /**
     * Set TimeFinish
     *
     * @param mixed $time_finish
     * @return \Home\Travel\Api\Data\TravelInterface
     */
    public function setTimeFinish($time_finish)
    {
        return $this->setData(self::TIME_FINISH, $time_finish);
    }

}