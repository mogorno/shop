<?php
namespace Home\Travel\Model;

use Home\Travel\Api\Data\TravelInterface;
use Home\Travel\Api\Data\TravelSearchResultsInterface;
use Home\Travel\Api\Data\TravelSearchResultsInterfaceFactory;
use Home\Travel\Api\TravelRepositoryInterface;
use Home\Travel\Model\TravelFactory as TravelFactory;
use Home\Travel\Model\ResourceModel\Travel as TravelResource;
use Home\Travel\Model\ResourceModel\Travel\Collection;
use Home\Travel\Model\ResourceModel\Travel\CollectionFactory as TravelCollectionFactory;
use Exception;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;

class TravelRepository implements TravelRepositoryInterface
{
    /**
     * @var TravelResource
     */
    protected $travelResource;

    /**
     * @var TravelFactory
     */
    protected $travelFactory;

    /**
     * @var travelCollectionFactory
     */
    protected $travelCollectionFactory;

    /**
     * @var TravelSearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;

    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;

    /**
     * TravelRepository constructor.
     * @param TravelResource $travelResource
     * @param \Home\Travel\Model\TravelFactory $travelFactory
     * @param TravelSearchResultsInterfaceFactory $searchResultsFactory
     * @param TravelCollectionFactory $travelCollectionFactory
     */
    public function __construct(
        TravelResource $travelResource,
        TravelFactory $travelFactory,
        TravelCollectionFactory $travelCollectionFactory,
        TravelSearchResultsInterfaceFactory $searchResultsFactory,
        CollectionProcessorInterface $collectionProcessor
    ) {
        $this->travelResource = $travelResource;
        $this->travelFactory = $travelFactory;
        $this->travelCollectionFactory = $travelCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->collectionProcessor = $collectionProcessor;
    }

    /**
     * Save tickets.
     *
     * @param TravelInterface $travel
     * @return TravelInterface
     * @throws LocalizedException
     */
    public function save(TravelInterface $travel)
    {
        try {
            $this->travelResource->save($travel);
        } catch (Exception $exception) {
            throw new CouldNotSaveException(
                __('Could not save the ticket: %1', $exception->getMessage()),
                $exception
            );
        }

        return $travel;
    }

    /**
     * Retrieve tickets.
     *
     * @param int $ticketId
     * @return TravelInterface
     * @throws LocalizedException
     */
    public function getById($ticketId)
    {
        $travel = $this->travelFactory->create();
        $travel->load($ticketId);
        if (!$travel->getId()) {
            throw new NoSuchEntityException(__('The CMS page with the "%1" ID doesn\'t exist.', $ticketId));
        }
        return $travel;
    }

    /**
     * Retrieve ticket matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     * @return TravelSearchResultsInterface
     * @throws LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        /** @var Collection $collection */
        $collection = $this->travelCollectionFactory->create();

        $this->collectionProcessor->process($searchCriteria, $collection);

        /** @var TravelSearchResultsInterface $searchResults */
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * Delete tickets.
     *
     * @param TravelInterface $travel
     * @return bool true on success
     * @throws LocalizedException
     */
    public function delete(TravelInterface $travel)
    {
        try {
            $this->travelResource->delete($travel);
        } catch (Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the page: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * Delete ticket by ID.
     *
     * @param int $ticketId
     * @return bool true on success
     * @throws NoSuchEntityException
     * @throws LocalizedException
     */
    public function deleteById($ticketId)
    {
        return $this->delete($this->getById($ticketId));
    }
}
