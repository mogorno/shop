<?php

namespace Home\Travel\Model\ResourceModel\Travel;

use Home\Travel\Api\Data\TravelInterface;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{

    /**
     * @var string
     */
    protected $_idFieldName = TravelInterface::TICKET_ID;

    /**
     * Define resource model
     *
     * @return void
     */

    protected function _construct()
    {
        $this->_init(\Home\Travel\Model\Travel::class, \Home\Travel\Model\ResourceModel\Travel::class);
    }
    /**
     * Add filter by travel
     *
     * @param int|array|\Magento\Store\Model\Store $store
     * @param bool $withAdmin
     * @return $this
     */
    public function addStoreFilter($store, $withAdmin = true)
    {
        return $this;
        if (!$this->getFlag('store_filter_added')) {
            $this->performAddStoreFilter($store, $withAdmin);
        }
        return $this;
    }
}
